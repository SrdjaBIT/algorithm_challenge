package main

import (
	"bufio"
	"fmt"
	"os"
	"sort"
	"strconv"
	"strings"

	"github.com/go-errors/errors"
)

/*

List Max

Parse an input string:

5 - size of an array [0,0,0,0,0]
4 - number of operations

1 2 3
4 5 6
7 8 9
11 11 11 - four operations entered

[[1 2 3] [4 5 6] [7 8 9] [11 11 11]] - entered data set printed out

Enter data:
5 4
1 2 3
4 5 6
7 8 9
11 11 11
[[1 2 3] [4 5 6] [7 8 9] [11 11 11]]

*/

func findMaxInList(firstInputLine, otherInputLines []int) (int, error) {
	maxIdx := len(otherInputLines) / 3

	elementsABKArray := make([][]int, maxIdx)
	for i := range elementsABKArray {
		elementsABKArray[i] = make([]int, 3)
	}

	k := 0

	for i := range otherInputLines {
		if i%3 == 0 {
			j := 0
			elementsABKArray[k][j] = otherInputLines[i]
			elementsABKArray[k][j+1] = otherInputLines[i+1]
			elementsABKArray[k][j+2] = otherInputLines[i+2]
			k++
		}
	}

	arrSize := firstInputLine[0]
	noOfOperations := firstInputLine[1]

	resArr := make([]int, arrSize)

	if arrSize >= 3 && arrSize <= 10000000 {
		if noOfOperations >= 1 && noOfOperations <= 200000 {
			for i := 0; i < noOfOperations; i++ {
				if elementsABKArray[i][0] >= 1 &&
					elementsABKArray[i][0] <= elementsABKArray[i][1] &&
					elementsABKArray[i][1] <= arrSize &&
					elementsABKArray[i][2] >= 0 && elementsABKArray[i][2] <= 1000000000 {
					for j := elementsABKArray[i][0]; j <= elementsABKArray[i][1]; j++ {
						resArr[j-1] += elementsABKArray[i][2]
					}
				} else {
					return -1, errors.New("Input error (integer size)")
				}
			}
		} else {
			return -1, errors.New("Input error (number of operations)")
		}

		fmt.Println(resArr)

		sort.Sort(sort.Reverse(sort.IntSlice(resArr)))

		return resArr[0], nil

	}
	return -1, errors.New("Input error (array size)")
}

func main() {
	reader := bufio.NewReader(os.Stdin)

	firstInputLineRead, _ := reader.ReadString('\n')

	firstInputLineList := strings.Fields(strings.Trim(firstInputLineRead, "\n"))

	var firstInputLine, otherInputLines []int

	for i := 0; i < 2; i++ {
		firstInputLineInt, _ := strconv.Atoi(firstInputLineList[i])
		firstInputLine = append(firstInputLine, firstInputLineInt)
	}

	for i := 0; i < firstInputLine[1]; i++ {
		otherInputLinesRead, _ := reader.ReadString('\n')
		elementsABKList := strings.Fields(strings.Trim(otherInputLinesRead, "\n"))

		for j := 0; j < len(elementsABKList); j++ {
			elementsABKInt, _ := strconv.Atoi(elementsABKList[j])
			otherInputLines = append(otherInputLines, elementsABKInt)
		}
	}

	max, err := findMaxInList(firstInputLine, otherInputLines)

	if err != nil {
		fmt.Println(err)
	} else {
		fmt.Println(max)
	}
}
