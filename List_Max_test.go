package main

import (
	"fmt"
	"strings"
	"testing"
)

func Test_findMaxInList(t *testing.T) {
	type args struct {
		firstInputLine  []int
		otherInputLines []int
	}
	tests := []struct {
		name    string
		args    args
		want    int
		wantErr interface{}
	}{
		// Test cases.
		{
			name: "Test_findMaxInList_1",
			args: args{
				firstInputLine:  []int{3, 1},
				otherInputLines: []int{1, 2, 3},
			},
			want:    3,
			wantErr: nil,
		},
		{
			name: "Test_findMaxInList_2",
			args: args{
				firstInputLine:  []int{5, 3},
				otherInputLines: []int{1, 2, 100, 2, 5, 100, 3, 4, 100},
			},
			want:    200,
			wantErr: nil,
		},
		{
			name: "Test_findMaxInList_3",
			args: args{
				firstInputLine:  []int{4, 3},
				otherInputLines: []int{2, 3, 603, 1, 1, 286, 4, 4, 882},
			},
			want:    882,
			wantErr: nil,
		},
		{
			name: "Test_findMaxInList_4",
			args: args{
				firstInputLine:  []int{2, 3},
				otherInputLines: []int{2, 3, 603, 1, 1, 286, 4, 4, 882},
			},
			want:    -1,
			wantErr: "Input error (array size)",
		},
		{
			name: "Test_findMaxInList_5",
			args: args{
				firstInputLine:  []int{10000001, 5},
				otherInputLines: []int{2, 3, 603, 1, 1, 286, 4, 4, 882},
			},
			want:    -1,
			wantErr: "Input error (array size)",
		},
		{
			name: "Test_findMaxInList_6",
			args: args{
				firstInputLine:  []int{3, 0},
				otherInputLines: []int{2, 3, 603, 1, 1, 286, 4, 4, 882},
			},
			want:    -1,
			wantErr: "Input error (number of operations)",
		},
		{
			name: "Test_findMaxInList_7",
			args: args{
				firstInputLine:  []int{3, 200001},
				otherInputLines: []int{2, 3, 603, 1, 1, 286, 4, 4, 882},
			},
			want:    -1,
			wantErr: "Input error (number of operations)",
		},
		{
			name: "Test_findMaxInList_8",
			args: args{
				firstInputLine:  []int{4, 3},
				otherInputLines: []int{2, 0, 603, 1, 1, 286, 4, 4, 882},
			},
			want:    -1,
			wantErr: "Input error (integer size)",
		},
		{
			name: "Test_findMaxInList_9",
			args: args{
				firstInputLine:  []int{4, 3},
				otherInputLines: []int{2, 604, 603, 1, 1, 286, 4, 4, 882},
			},
			want:    -1,
			wantErr: "Input error (integer size)",
		},
		{
			name: "Test_findMaxInList_10",
			args: args{
				firstInputLine:  []int{4, 3},
				otherInputLines: []int{2, 3, 603, 1, 1, 286, 5, 4, 882},
			},
			want:    -1,
			wantErr: "Input error (integer size)",
		},
		{
			name: "Test_findMaxInList_11",
			args: args{
				firstInputLine:  []int{4, 3},
				otherInputLines: []int{2, 3, 0, 1, 1, 286, 5, 4, 882},
			},
			want:    -1,
			wantErr: "Input error (integer size)",
		},
		{
			name: "Test_findMaxInList_12",
			args: args{
				firstInputLine:  []int{4, 3},
				otherInputLines: []int{2, 3, 1000000001, 1, 1, 286, 3, 4, 882},
			},
			want:    -1,
			wantErr: "Input error (integer size)",
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			got, err := findMaxInList(tt.args.firstInputLine, tt.args.otherInputLines)

			switch err.(type) {
			case error:
				fmt.Println(err)
				if strings.Compare(err.Error(), tt.wantErr.(string)) != 0 {
					t.Errorf("error = %v, wantErr %v", err, tt.wantErr)
				}
			default:
				if err != tt.wantErr {
					t.Errorf("error = %v, wantErr %v", err, tt.wantErr)
				}
			}

			if got != tt.want {
				t.Errorf("findMaxInList() = %v, want %v", got, tt.want)
			}
		})
	}
}
