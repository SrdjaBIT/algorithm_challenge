#!/usr/bin/env bash

# Add it to pre-commit hook:
# echo -e "#\!/usr/bin/env bash\n\n./misc/scripts/pre-commit.sh" > .git/hooks/pre-commit && chmod +x .git/hooks/pre-commit

gofiles=`find -name "*.go" -not -path "./vendor/*"`

echo "goimports"
goimports -w ${gofiles}

echo "gofmt"
gofmt -s -w ${gofiles}

echo "go vet"
go tool vet . 2>&1 | grep -v vendor

echo "gocyclo"
gocyclo -over 15 ${gofiles}

echo "golint"
lint_errors=`golint ./... | grep -v vendor`
if [[ ${lint_errors} ]]; then
    echo "golint find problems:"
    echo "$lint_errors"
    exit 1
fi

echo "ineffassign"
ineffassign ${gofiles}

echo "misspell"
misspell *.md ${gofiles}templates/*.tpl

git add `git ls-files -m`

echo "go test"
go test ./...